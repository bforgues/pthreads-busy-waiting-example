### Calculating Pi with pThreads ###


To compile and run with Docker(get docker login error when running second line but was able to successfully run make file):
```
docker run --rm -v ($pwd):/tmp -w /tmp/Default rgreen13/alpine-bash-gpp make all
docker run --rm -v ($pwd):/tmp -w /tmp/Default rgreen13/alpine-bash-bpp ./pThreads_Timing
```


1. The main topic discussed in class this week and practiceed in our excercise was one of the ways to execute parrallel programs.
 This was through the busy-waiting paradigm. This paradigm essentially forces all cores to wait their turn to execute a function.

2. Well the most important aspect of this excercise to note is for the first time we were given permission or rather required to 
global variables. Without the global variable the busy-waiting paradigm would not have worked very well so it was needed.
The parrallel work still exectues in similar time as we saw in class. The only difference is the global variable so this can be
seen by simply looking at the code. 

3. This excercise was not too difficult to understand. Well atleast after the explanation in class on Thursday. The global variable 
is handled by the function called by the threads. It also must be set to zero inside the second for loop that iterates over the trials.
So the hardest part was learning how to change and control the global variable since I have never used one before in serious code.

4. So I was unable to perform my own personal expirements due to docker issues that I will be asking you about after class. But the main 
difference between the previous code and this code is the lack of a returning variable. In the previous code we returned the estimate that
each core produced. In this code, the cores all change the same global variable and thus we had to impliment the busy-waiting paradigm.
This allowed each core to access the variable separately without overwriting each other. The optimization does some really cool stuff
when turned on and off. Thanks to the busy-waiting paradigm, the myRank loop must be cleared before the cores can edit the global variable.
However, when optimization is turned on, the compiler moves the check loop and the cores all overwrite the data. We learned through the use of
the volatile keyword, we can keep the compiler from changing anything related to those variables. Once this is done, the optimizer can be enabled
without detrimate to the results. In fact, though the improvement is marginal, the time is faster when optimization and the volatile keyword
is being used. 