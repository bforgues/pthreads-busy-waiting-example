
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h> 
#include <iostream>
#include <iomanip>
#include <streambuff>
#include <unistd.h>
#include "CStopWatch.h"

double pi_estimate = 0;
int flag;

struct dataToPass{
   int id;
   int numSamples;
};


/*-------------------------------------------------------------------*/
void* getSamples(void* data) {

   dataToPass* myData = (dataToPass*)data;

   int numSamples = myData->numSamples;
   int myRank = myData->id;

   double x, y;
   //double* estimate = new double();
   //*numSamples = (long)nSamples;

   int number_in_circle = 0.00;
   //int number_of_tosses = 0;

   for (toss = 0; toss < numSamples; toss++) {
      int x = (double) std::rand() / (double) RAND_MAX * 2.0 - 1.0;
      int y = (double) std::rand() / (double) RAND_MAX * 2.0 - 1.0;

      //int distance_squared = x*x + y*y;

         if (x*x + y*y <= 1){
            number_in_circle++;
         }
   }

   while (flag != tId);

   pi_estimate = pi_estimate + (4.0*number_in_circle/numSamples);  

   delete myData;
   return NULL;
}

int main() {
   long        threadMin, threadMax, threadStep;
   long        sampleMin, sampleMax, sampleStep;
   long        numTrials;
   pthread_t*  thread_handles; 
   CStopWatch  timer;


   dataToPass* d;
   threadMin  = 10; 
   threadMax  = 100;
   threadStep = 10;

   sampleMin  = 1000;
   sampleMax  = 10000;
   sampleStep = 1000;

   numTrials  = 10;
   
   thread_handles = new pthread_t[threadMax]; 

   
   for(int numThreads=threadMin; numThreads<threadMax; numThreads+=threadStep){

      for(long numSamples=sampleMin; numSamples < sampleMax; numSamples+=sampleStep){
         for(int curTrial=0; curTrial < numTrials; curTrial++){
            pi_estimate = 0.0;
            flag = 0;
            timer.startTimer();
            for (long thread = 0; thread < numThreads; thread++){

               d = new dataToPass();
               d->id = thread;
               d->numSamples = numSamples/numThreads;

               pthread_create(&thread_handles[thread], NULL, getSamples, (void*) myData);  
            }

            for (int thread = 0; thread < numThreads; thread++){
               
               pthread_join(thread_handles[thread], NULL);
            }
            estimate /= (double)numThreads;
            timer.stopTimer();

             std::cout << std::fixed << std::setprecision(4) << estimate << " " << std::setprecision(0) << numThreads << " " << numSamples << " " << std::setprecision(4)<< timer.getElapsedTime() << "\n";
         }
      }
   }

   delete thread_handles;

   return 0;
} 